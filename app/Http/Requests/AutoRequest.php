<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class AutoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'state_number' => 'required|string',
            'color' => 'required|string',
            'vin_code' => 'required|max:17',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'A name is empty',
            'state_number.required' => 'A state_number is empty',
            'color.required' => 'A color is empty',
            'vin_code.required' => 'A vin code is empty',
            'vin_code.max:17' => 'A vin code > 17',
        ];
    }
}
